require './lib/sorting_in_ruby.rb'

array_generator = ArrayElementGenerator.new(ElementGenerator.new)
qs_benchmark = SortBenchmark.new('quick_sort.log')

ARRAY_TYPES.each do |array_type|
  Parallel.each(ARRAY_SIZES, in_threads: Parallel.processor_count) do |array_size|
    sorter = Sorter.new(array_generator.generate(array_type, array_size))
    qs_benchmark.log('quick_sort', array_type.to_s, array_size) { sorter.quick_sort }
  end
end