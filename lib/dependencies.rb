require_relative './bubble_sort.rb'
require_relative './quick_sort.rb'
require 'benchmark'
require 'logger'

def check_sort!(array)
  array.each.with_index do |num, index|
    previous_index = index - 1

    next if previous_index < 0
    next if array[previous_index] <= num

    raise "#{array[previous_index]}[index=#{previous_index}] < #{num}[index=#{index}]"
  end

  true
end

class ElementGenerator

  def degenerated
    rand(1..12)
  end

  def random
    rand(0..70_000)
  end

end

class ArrayElementGenerator

  def initialize(element_generator)
    @element_generator = element_generator
  end

  def generate(type, size)
    case type
    when :liner then liner(size)
    when :reversed_liner then liner(size).reverse
    when :degenerated then sequence(size) { element_generator.degenerated }
    when :random then sequence(size) { element_generator.random }
    else
      raise 'Unknown type'
    end
  end

  private

  attr_reader :element_generator

  def liner(size)
    size.times.map(&:itself)
  end

  def sequence(size, &block)
    size.times.map { block.call }
  end

end

class SortBenchmark

  def initialize(filename)
    File.delete(filename) if File.exist?(filename)
    @logger = File.open(filename, 'a')
    @mutex = Thread::Mutex.new
  end

  def log(algorithm, type, size, &block)
    result = nil
    measure = Benchmark.realtime { result = block.call }.round(6)
    check_sort!(result)

    msg = "#{algorithm.ljust(11)} for #{type.ljust(14)} of #{size.to_s.ljust(7)} : #{measure.to_s.ljust(8)} s"
    log_puts(msg)
  end

  private

  attr_reader :logger, :mutex

  def log_puts(msg)
    mutex.synchronize do
      puts msg
      logger.puts msg
    end
  end

end

class Sorter

  def initialize(collection)
    @collection = collection
  end

  def bubble_sort
    BubbleSort.new.call(collection)
  end

  def quick_sort
    QuickSort.new.call(collection)
  end

  private

  attr_reader :collection

end
