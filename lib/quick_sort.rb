class QuickSort

  def call(arr, lo = nil, hi = nil)
    lo ||= 0
    hi ||= arr.size - 1

    quick_sort(arr, lo, hi)
  end

  private

  def quick_sort(arr, lo, hi)
    if arr.length <= 1
      return arr
    else
      pivot = partition(arr, lo, hi)
      if (lo < pivot - 1)
        quick_sort(arr, lo, pivot-1)
      end
      if (hi > pivot)
        quick_sort(arr, pivot, hi)
      end
      return arr
    end
  end

  # partition function (selects a pivot, sorts into partitions, and returns the array index of the pivot)
  def partition(arr, lo, hi)
    pivot = arr[hi]
    left = lo

    for element in (lo...hi)
      if arr[element] <= pivot
        arr[left], arr[element] = arr[element], arr[left]
        left += 1
      end
    end

    arr[left], arr[hi] = arr[hi], arr[left]

    left
  end

end
