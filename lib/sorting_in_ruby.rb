require_relative './dependencies.rb'
require 'bundler'

Bundler.require

ARRAY_TYPES = %i[liner reversed_liner degenerated random]
ARRAY_SIZES = [1000, 2000, 4000, 8000, 16000, 32000, 64000, 128000, 512000, 1024000]
